# Rogue Trader 
An **UNOFFICIAL** system for playing Rogue Trader [Foundry VTT](https://foundryvtt.com/).

It provides support for **character sheets only**, game content should be drawn from official source books.

This project is derived from Moo Man's Dark Heresy 2E project and is released under the GPL-3.0 License.

## Install
1. Go to the setup page and choose **Game Systems**.
2. Click the **Install System** button, and paste in this [manifest link](https://gitlab.com/ogre92/rogue-trader/raw/main/system.json)
3. Create a Game World using the Dark Heresy system.

## Related Website
- https://foundryvtt.com/
- https://www.drivethrurpg.com/product/65991/Rogue-Trader-Core-Rulebook

## Licence
[GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/)
