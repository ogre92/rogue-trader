export async function commonRoll(rollData) {
    await _computeTarget(rollData);
    await _rollTarget(rollData);
    await _sendToChat(rollData);
}

export async function combatRoll(rollData) {
    await _computeTarget(rollData);
    await _rollTarget(rollData);
    if (rollData.isSuccess) {
        await _rollDamage(rollData);
    }
    await _sendToChat(rollData);
}

export async function reportEmptyClip(rollData) {
    await _emptyClipToChat(rollData);
}

async function _computeTarget(rollData) {
    const range = (rollData.range) ? rollData.range : "0";
    let attackType = 0;
    if (typeof rollData.attackType !== "undefined" && rollData.attackType != null) {
        _computeRateOfFire(rollData);
        attackType = rollData.attackType.modifier;
    }
    let psyModifier = 0;
    if (typeof rollData.psy !== "undefined" && typeof rollData.psy.useModifier !== "undefined" && rollData.psy.useModifier) {
        psyModifier = (rollData.psy.max - rollData.psy.value) * 10;
        rollData.psy.push = psyModifier < 0;
    }
    const formula = `0 + ${rollData.modifier} + ${range} + ${attackType} + ${psyModifier}`;
    let r = new Roll(formula, {});
    await r.evaluate();
    if (r.total > 60) {
        rollData.target = rollData.baseTarget + 60;
    } else if (r.total < -60) {
        rollData.target = rollData.baseTarget + -60;
    } else {
        rollData.target = rollData.baseTarget + r.total;
    }
    rollData.rollObject = r;
}

async function _rollTarget(rollData) {
    //Check to see if weapon is a flamer.  Auto hit with a 5 so the location will always be body with no DOS else do normal attack resolution.
    if (rollData.weaponType === "Flame"){
        rollData.isSuccess = true;
        rollData.result = 5;
        rollData.dos = 0;
        rollData.dof = 0;
    } else {
        let r = new Roll("1d100", {});
        await r.evaluate();
        rollData.result = r.total;
        rollData.rollObject = r;
        rollData.isSuccess = rollData.result <= rollData.target;
        if (rollData.isSuccess) {
            rollData.dof = 0;
            rollData.dos = 0 + _getDegree(rollData.target, rollData.result);
        } else {
            rollData.dos = 0;
            rollData.dof = 0 + _getDegree(rollData.result, rollData.target);
        }
        if (typeof rollData.psy !== "undefined") _computePsychicPhenomena(rollData);
    }   
}

async function _rollDamage(rollData) {
    let formula = "0";
    rollData.damages = [];
    if (rollData.damageFormula) formula = `${rollData.damageFormula} + ${rollData.damageBonus}`;
    let penetration = await _rollPenetration(rollData);
    let firstHit = await _computeDamage(formula, rollData.dos, penetration, rollData.target, rollData.weaponType);
    if (firstHit.total !== 0) {
        const firstLocation = _getLocation(rollData.result);
        firstHit.location = firstLocation;
        rollData.damages.push(firstHit);
        if (rollData.attackType.hitMargin > 0) {
            let maxAdditionalHit = Math.floor((rollData.dos - 1) / rollData.attackType.hitMargin);
            if (typeof rollData.maxAdditionalHit !== "undefined" && maxAdditionalHit > rollData.maxAdditionalHit) {
                maxAdditionalHit = rollData.maxAdditionalHit;
            }
            rollData.numberOfHit = maxAdditionalHit + 1;
            for (let i = 0; i < maxAdditionalHit; i++) {
                let additionalHit = await _computeDamage(formula, rollData.dos, penetration, rollData.target, rollData.weaponType);
                additionalHit.location = _getAdditionalLocation(firstLocation, i);
                rollData.damages.push(additionalHit);
            }
        } else {
            rollData.numberOfHit = 1;
        }
        let minDamage = rollData.damages.reduce((min, damage) => min.minDice < damage.minDice ? min : damage, rollData.damages[0]);
        if (minDamage.minDice < rollData.dos) {
          minDamage.total += (rollData.dos - minDamage.minDice)
          minDamage.replaced = true;
        };
    }
}

async function _computeDamage(formula, dos, penetration, targetNumber, gunType) {
    let r = new Roll(formula, {});
    await r.evaluate();
    let damage = {
        total: r.total,
        righteousFury: 0,
		righteousFuryHitDetails: null,
		righteousFuryDamageDetails: "",
		righteousFuryTotalDamage: 0,
        penetration: penetration,
        dices: [],
        result: 0,
        dos: dos,
        formula: formula,
        replaced: false,
        resultView: ""
    };
 
	  for (let term of r.terms) {
        if (typeof term === 'object' && term !== null && term.results) {
			  for (let result of term.results){
                
				if (result.active && result.result === term.faces){
					let rf = await _rollRighteousFury(targetNumber, gunType);
					damage.righteousFury = rf.damage;
					damage.righteousFuryDamageDetails = rf.damageDetails;
					damage.righteousFuryHitDetails = rf.hitDetails;
				}
				
                if (result.active && result.result < dos) damage.dices.push(result.result);
                if (result.active && (typeof damage.minDice === "undefined" || result.result < damage.minDice)) damage.minDice = result.result;
                damage.result = result.result;
				
				//Build string to display each die roll
				damage.resultView += result.result + ", ";
            };
        }
    };
	
	//Remove last comma
	damage.resultView = damage.resultView.slice(0, -2);
	let addDOS = 0;
	if (damage.dices.length > 0) addDOS = dos - damage.minDice;
	damage.righteousFuryTotalDamage = damage.total + addDOS + damage.righteousFury;
	
	return damage;
}

async function _rollPenetration(rollData) {
    let penetration = (rollData.penetrationFormula) ? rollData.penetrationFormula : "0";
    if (penetration.includes("("))
    {
        if (rollData.dos >= 3)
            penetration = parseInt(penetration.split("(")[1])
        else penetration = parseInt(penetration)
    }
    let r = new Roll(penetration.toString(), {});
    await r.evaluate();
    return r.total;
}

async function _rollRighteousFury(targetNumber, gunType) {
	var rfr = 0;
	
	let rfDetails = {
		damage: 0,
		hitDetails: "",
		damageDetails:"",
	}
	
	do {
        //If weaponType is Flame don't roll to confirm RF and force to hit roll to always be 5 so it will hit the Body location.
        if (gunType === "Flame"){
            rfDetails.hitDetails += 5 + ", ";
            let r = new Roll("1d10", {});
            await r.evaluate();
            rfDetails.damageDetails += r.total + ", ";
            rfDetails.damage += r.total;
            rfr = r.total;
        }else{
            //Normal RF confirmation roll.
            let rfc = await _rollRighteousFuryConfirm(targetNumber);
            rfDetails.hitDetails += rfc.rolled + ", ";
            if (rfc.hit){
                let r = new Roll("1d10", {});
                await r.evaluate();
                rfDetails.damageDetails += r.total + ", ";
                rfDetails.damage += r.total;
                rfr = r.total;
            }
        }
	}
	while (rfr == 10);
		
	rfDetails.hitDetails = rfDetails.hitDetails.slice(0, -2);
	rfDetails.damageDetails = rfDetails.damageDetails.slice(0, -2);
    return rfDetails;
}

async function _rollRighteousFuryConfirm(targetNumber) {
	let r = new Roll("1d100", {});
	await r.evaluate();
	
	let rcDetails = {
		rolled: 0,
		hit: 0
	}
	
	rcDetails.hit = r.total <= targetNumber
	rcDetails.rolled = r.total;
	return rcDetails;
}

function _computePsychicPhenomena(rollData) {
    rollData.psy.hasPhenomena = rollData.psy.push ? !_isDouble(rollData.result) : _isDouble(rollData.result);
}

function _isDouble(number) {
    if (number === 100) {
        return true;
    } else {
        const digit = number % 10;
        return number - digit === digit * 10;
    }
}

function _getLocation(result) {
    const toReverse = result < 10 ? "0" + result : result.toString();
    const locationTarget = parseInt(toReverse.split('').reverse().join(''));
    if (locationTarget <= 10) {
        return "ARMOUR.HEAD";
    } else if (locationTarget <= 20) {
        return "ARMOUR.RIGHT_ARM";
    } else if (locationTarget <= 30) {
        return "ARMOUR.LEFT_ARM";
    } else if (locationTarget <= 70) {
        return "ARMOUR.BODY";
    } else if (locationTarget <= 85) {
        return "ARMOUR.RIGHT_LEG";
    } else if (locationTarget <= 100) {
        return "ARMOUR.LEFT_LEG";
    } else {
        return "ARMOUR.BODY";
    }
}

function _computeRateOfFire(rollData) {
    rollData.maxAdditionalHit = 0;
    if (rollData.attackType.name === "semi_auto" || rollData.attackType.name === "barrage") {
        rollData.attackType.modifier = 10;
        rollData.attackType.hitMargin = 2;
        rollData.maxAdditionalHit = rollData.rateOfFire.burst - 1;
    } else if (rollData.attackType.name === "full_auto" || rollData.attackType.name === "storm") {
        rollData.attackType.modifier = 20;
        rollData.attackType.hitMargin = 1;
        rollData.maxAdditionalHit = rollData.rateOfFire.full - 1;
    } else if (rollData.attackType.name === "called_shot") {
        rollData.attackType.modifier = -20;
        rollData.attackType.hitMargin = 0;
    } else {
        rollData.attackType.modifier = 0;
        rollData.attackType.hitMargin = 0;
    }
}

const additionalHit = {
    head: ["ARMOUR.HEAD", "ARMOUR.RIGHT_ARM", "ARMOUR.BODY", "ARMOUR.LEFT_ARM", "ARMOUR.BODY"],
    rightArm: ["ARMOUR.RIGHT_ARM", "ARMOUR.RIGHT_ARM", "ARMOUR.HEAD", "ARMOUR.BODY", "ARMOUR.RIGHT_ARM"],
    leftArm: ["ARMOUR.LEFT_ARM", "ARMOUR.LEFT_ARM", "ARMOUR.HEAD", "ARMOUR.BODY", "ARMOUR.LEFT_ARM"],
    body: ["ARMOUR.BODY", "ARMOUR.RIGHT_ARM", "ARMOUR.HEAD", "ARMOUR.LEFT_ARM", "ARMOUR.BODY"],
    rightLeg: ["ARMOUR.RIGHT_LEG", "ARMOUR.BODY", "ARMOUR.RIGHT_ARM", "ARMOUR.HEAD", "ARMOUR.BODY"],
    leftLeg: ["ARMOUR.LEFT_LEG", "ARMOUR.BODY", "ARMOUR.LEFT_ARM", "ARMOUR.HEAD", "ARMOUR.BODY"],
}

function _getAdditionalLocation(firstLocation, numberOfHit) {
    if (firstLocation === "ARMOUR.HEAD") {
        return _getLocationByIt(additionalHit.head, numberOfHit);
    } else if (firstLocation === "ARMOUR.RIGHT_ARM") {
        return _getLocationByIt(additionalHit.rightArm, numberOfHit);
    } else if (firstLocation === "ARMOUR.LEFT_ARM") {
        return _getLocationByIt(additionalHit.leftArm, numberOfHit);
    } else if (firstLocation === "ARMOUR.BODY") {
        return _getLocationByIt(additionalHit.body, numberOfHit);
    } else if (firstLocation === "ARMOUR.RIGHT_LEG") {
        return _getLocationByIt(additionalHit.rightLeg, numberOfHit);
    } else if (firstLocation === "ARMOUR.LEFT_LEG") {
        return _getLocationByIt(additionalHit.leftLeg, numberOfHit);
    } else {
        return _getLocationByIt(additionalHit.body, numberOfHit);
    }
}

function _getLocationByIt(part, numberOfHit) {
    const index = numberOfHit > (part.length - 1) ? part.length - 1 : numberOfHit;
    return part[index];
}


function _getDegree(a, b) {
    return Math.floor((a - b) / 10);
}

async function _sendToChat(rollData) {
    const html = await renderTemplate("systems/rogue-trader/template/chat/roll.html", rollData);
    let chatData = {
        user: game.user.id,
        rollMode: game.settings.get("core", "rollMode"),
        content: html,
        type: CONST.CHAT_MESSAGE_TYPES.ROLL
    };
    if(rollData.rollObject){
        chatData.roll = rollData.rollObject;

    }
    if (["gmroll", "blindroll"].includes(chatData.rollMode)) {
        chatData.whisper = ChatMessage.getWhisperRecipients("GM");
    } else if (chatData.rollMode === "selfroll") {
        chatData.whisper = [game.user];
    }
    ChatMessage.create(chatData);
}

async function _emptyClipToChat(rollData) {
    let chatData = {
        user: game.user.id,
        content: `
          <div class="rogue-trader chat roll">
              <div class="background border">
                  <p><strong>Reload! Out of Ammo!</strong></p>
              </div>
          </div>
        `
    };
    ChatMessage.create(chatData);
}